import qsharp
from random_gen import random_num_gen

max = 50 
output = max + 1
while output > max:
    bit_string = [] 
    for i in range(0, max.bit_length()):
        bit_string.append(random_num_gen.simulate()) 
        
    output = int("".join(str(x) for x in bit_string), 2) 

print("The random number generated is :" + str(output))

for j in range(0,len(bit_string)):
    print(bit_string[j])