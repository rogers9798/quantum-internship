//day1 04-05-2020

namespace HelloSachin{
    open Microsoft.Quantum.Intrinsic;
    open Microsoft.Quantum.Canon;

    operation Hello() : Result {
        Message("hey there quantum world!");
        return Zero;
    }
}