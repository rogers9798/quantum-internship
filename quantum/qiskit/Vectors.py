#!/usr/bin/env python
# coding: utf-8

# In[2]:


from qiskit import *


# In[10]:


from qiskit.tools.visualization import plot_bloch_multivector


# In[11]:


crct=QuantumCircuit(1,1)
crct.x(0) #applied to that qubit
simulator=Aer.get_backend('statevector_simulator') #statevector - vector that describes the quantum state of qubit


# In[12]:


res=execute(crct, backend=simulator).result()


# In[15]:


statevec=res.get_statevector() # storing results in statevec var
print(statevec) # output performed on as cx on '0' qubit


# In[16]:


get_ipython().run_line_magic('matplotlib', 'inline')
crct.draw(output='mpl') #output = statevector [0.+0.j 1.+0.j]


# In[17]:


plot_bloch_multivector(statevec) # X gate took state |0> to |1> using bloch_vector


# MEASURING THE CIRCUIT AND ITS PROBABILITY

# In[18]:


crct.measure([0],[0]) # measure qubit 0 and keep in classical bit 0


# In[20]:


backend=Aer.get_backend('qasm_simulator')


# In[21]:


results=execute(crct, backend=backend, shots=1024).result()


# In[22]:


counts=results.get_counts()
from qiskit.tools.visualization import plot_histogram


# In[23]:


plot_histogram(counts)


# MATRIX REPRESENTATION OF THE CIRCUIT

# In[24]:


crct=QuantumCircuit(1,1)
crct.x(0) #applied to that qubit
simulator=Aer.get_backend('unitary_simulator') #unitary - vector that describes the quantum state of qubit in matrix form


# In[25]:


res=execute(crct, backend=simulator).result()


# In[26]:


unitary=res.get_unitary() # storing results in unitary var
print(unitary) # output as X gate

get_ipython().run_line_magic('matplotlib', 'inline')
crct.draw(output='mpl') 


# In[ ]:




