from qiskit import *
from qiskit.tools.visualization import plot_bloch_multivector
from qiskit.tools.visualization import plot_bloch_vector


def main():

    crct = QuantumCircuit(1, 1)
    crct.x(0)  # applied to that qubit
    simulator = Aer.get_backend(
        "statevector_simulator"
    )  # statevector - vector that describes the quantum state of qubit

    res = execute(crct, backend=simulator).result()
    statevec = res.get_statevector()  # storing results in statevec var
    print(statevec)  # output performed on as cx on '0' qubit

    import matplotlib

    crct.draw(output="mpl")  # output = statevector [0.+0.j 1.+0.j]

    plot_bloch_multivector(
        statevec
    )  # X gate took state |0> to |1> using bloch_multivector

    # MEASURING THE CIRCUIT AND ITS PROBABILITY

    crct.measure([0], [0])  # measure qubit 0 and keep in classical bit 0

    backend = Aer.get_backend("qasm_simulator")

    results = execute(crct, backend=backend, shots=1024).result()

    counts = results.get_counts()
    from qiskit.tools.visualization import plot_histogram

    plot_histogram(counts)

    # MATRIX REPRESENTATION OF THE CIRCUIT

    crct = QuantumCircuit(1, 1)
    crct.x(0)  # applied to that qubit
    simulator = Aer.get_backend(
        "unitary_simulator"
    )  # unitary - vector that describes the quantum state of qubit in matrix form

    res = execute(crct, backend=simulator).result()

    unitary = res.get_unitary()  # storing results in unitary var
    print(unitary)  # output as X gate

    crct.draw(output="mpl")
    plot_bloch_multivector(
        unitary
    )  # bloch_multivector representaion of unitary [matrix form]


main()
